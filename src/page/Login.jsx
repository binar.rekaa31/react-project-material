import React, { Component } from 'react';
import { loginService } from '../services/login';

class Login extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            email : "",
            password : "",
            message: "",
            token: localStorage.getItem('token') || "",
            isLogin: false
        }
    }

    handleEmailInput = (event) => {
        this.setState({
            email : event.target.value
        })
    }

    handlePassInput = (event) => {
        this.setState({
            password : event.target.value
        })
    }

    handleSubmit = async () => {
        let objInput = {
            email : this.state.email,
            password : this.state.password
        }

        loginService(objInput).then((res) => {
            localStorage.setItem("token", res.token)
        })
    }

    handleLogout = () => {
        localStorage.clear()
    }
    
    render() {
        return (
            <div>
                <h1>{this.state.message}</h1>
                <input type="email" onChange={(event) => this.handleEmailInput(event)}/>
                <input type="password" onChange={(event) => this.handlePassInput(event)}/>
                <button onClick={() => this.handleSubmit()}>Login</button>
            </div>
        );
    }
}

export default Login;
