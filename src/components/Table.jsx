import React, { Component } from 'react';

class Table extends Component {
    render() {
        return (
            <>
                <h1>{this.props.tableName}</h1>
                <table >
                    <th>
                        <td>No</td>
                        {this.props.listColumnName.map((item, index) => 
                            <td key={index}>{item}</td>
                        )}
                    </th>
                    {this.props.listData.map((row, index1) => 
                        <>
                            <tr>
                                <td key={index1}>{index1+1}</td>
                                {this.props.listColumnName.map((item, index2) => 
                                    <td key={index2}>{row[item]}</td>
                                )}
                            </tr>
                        </>
                    )}
                </table>
            </>
        );
    }
}

export default Table;
