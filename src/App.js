import { Link, Route, Routes } from "react-router-dom";
import Home from "./page/Home";
import Login from "./page/Login";
import Post from "./page/Post";
import PrivateRoute from "./routes/PrivateRoute";

function App() {
	return (
		<div className="App">

			<ul>
				<li><Link to={'/'}>Home</Link></li>
				<li><Link to={'/post'}>Post</Link></li>
			</ul>

			<Routes>
				
				<Route path="/" element={
					<PrivateRoute>
						<Home />
					</PrivateRoute>
				} />

				<Route path="/login" element={<Login />} />
				
				<Route path="/post" element={
					<PrivateRoute>
						<Post />
					</PrivateRoute>
				} />

			</Routes>
		</div>
	);
}

export default App;
