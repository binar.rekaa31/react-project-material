import axios from "axios";

export const loginService = (data) => {
    return axios.post("http://localhost:3080/api/v1/auth", data).then((response) => {
        return response.data
    }).catch((error) => {
        return error
    })
}